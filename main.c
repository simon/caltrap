/*
 * main.c: command line parsing and top level
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "caltrap.h"

/* Global configuration variables exported to the rest of the code */
const char *dirpath, *dbpath, *pager;

static enum { COLOUR_NO, COLOUR_TTY, COLOUR_YES } colour = COLOUR_TTY;

int use_vt_colour(int fd)
{
    if (colour == COLOUR_NO)
        return 0;
    if (colour == COLOUR_TTY && !isatty(fd))
        return 0;
    if (!is_ecma_term())
        return 0;
    return 1;
}

int main(int argc, char **argv) {
    int nogo;
    int errs;
    int verbose;
    struct entry e;
    enum { NONE, INIT, ADD, LIST, CRON, DUMP, LOAD, INFO, EDIT, DEL } command;
    char *args[4];
    int nargs = 0;
    int machine_readable = FALSE;
    char *wp;

    /*
     * Set up initial (default) parameters.
     */
    nogo = errs = verbose = FALSE;
    e.type = INVALID_TYPE;
    e.length = e.period = INVALID_DURATION;
    e.sd = e.ed = INVALID_DATE;
    e.st = e.et = INVALID_TIME;
    e.description = NULL;

    {
        const char *homedir;
        char *cfgpath;
        FILE *fp;

        homedir = getenv("HOME");
        if (homedir == NULL) homedir = "/";
        dirpath = wp = smalloc(strlen(homedir) + 20);
        sprintf(wp, "%s%s.caltrap", homedir,
                homedir[strlen(homedir)-1] == '/' ? "" : "/");
        dbpath = wp = smalloc(strlen(dirpath) + 20);
        sprintf(wp, "%s/db", dirpath);
        cfgpath = wp = smalloc(strlen(dirpath) + 20);
        sprintf(wp, "%s/config", dirpath);
        pager = NULL;

        if ((fp = fopen(cfgpath, "r")) != NULL) {
            int lineno = 0;
            char line[4096], *p, *cmd;
            while (fgets(line, sizeof(line), fp)) {
                lineno++;
                line[strcspn(line, "\r\n")] = '\0';
                p = line;
                while (*p && isspace((unsigned char)*p)) p++;
                if (!*p || *p == '#')
                    continue;
                cmd = p;
                while (*p && !isspace((unsigned char)*p)) p++;
                if (*p) *p++ = '\0';
                while (*p && isspace((unsigned char)*p)) p++;
                if (!strcmp(cmd, "pager")) {
                    pager = wp = smalloc(strlen(p)+1);
                    strcpy(wp, p);
                } else {
                    fatalerr_badconfigcmd(cfgpath, lineno, cmd);
                }
            }
            fclose(fp);
        }

        sfree(cfgpath);
    }

    command = NONE;

    /*
     * Parse command line arguments.
     */
    while (--argc) {
	char *p = *++argv;
	if (*p == '-') {
	    /*
	     * An option.
	     */
	    while (p && *++p) {
		char c = *p;
		switch (c) {
		  case '-':
		    /*
		     * Long option.
		     */
		    {
			char *opt, *val;
			opt = p++;     /* opt will have _one_ leading - */
			while (*p && *p != '=')
			    p++;	       /* find end of option */
			if (*p == '=') {
			    *p++ = '\0';
			    val = p;
			} else
			    val = NULL;
			if (!strcmp(opt, "-help")) {
			    help();
			    nogo = TRUE;
			} else if (!strcmp(opt, "-init")) {
			    command = INIT;
			} else if (!strcmp(opt, "-info")) {
			    command = INFO;
			} else if (!strcmp(opt, "-edit")) {
			    command = EDIT;
			} else if (!strcmp(opt, "-delete")) {
			    command = DEL;
			} else if (!strcmp(opt, "-dump")) {
			    command = DUMP;
			} else if (!strcmp(opt, "-load")) {
			    command = LOAD;
			} else if (!strcmp(opt, "-colour") ||
                                   !strcmp(opt, "-color")) {
                            if (!val)
                                errs = TRUE, err_optnoarg(opt);
                            else if (!strcmp(val, "yes") ||
                                     !strcmp(val, "on") ||
                                     !strcmp(val, "always"))
                                colour = COLOUR_YES;
                            else if (!strcmp(val, "auto") ||
                                     !strcmp(val, "tty"))
                                colour = COLOUR_TTY;
                            else if (!strcmp(val, "no") ||
                                     !strcmp(val, "off") ||
                                     !strcmp(val, "never"))
                                colour = COLOUR_NO;
                            else
                                errs = TRUE, err_badcolouropt(val);
			} else if (!strcmp(opt, "-machine-readable")) {
                            machine_readable = TRUE;
			} else if (!strcmp(opt, "-version")) {
			    showversion();
			    nogo = TRUE;
			} else if (!strcmp(opt, "-licence") ||
				   !strcmp(opt, "-license")) {
			    licence();
			    nogo = TRUE;
			}
			/*
			 * A sample option requiring an argument:
			 * 
			 * else if (!strcmp(opt, "-output")) {
			 *     if (!val)
			 *         errs = TRUE, err_optnoarg(opt);
			 *     else
			 *         ofile = val;
			 * }
			 */
			else {
			    errs = TRUE, err_nosuchopt(opt);
			}
		    }
		    p = NULL;
		    break;
		  case 'h':
		  case 'V':
		  case 'L':
		  case 'a':
		  case 'l':
		  case 'C':
		  case 'v':
		    /*
		     * Option requiring no parameter.
		     */
		    switch (c) {
		      case 'h':
			help();
			nogo = TRUE;
			break;
		      case 'V':
			showversion();
			nogo = TRUE;
			break;
		      case 'L':
			licence();
			nogo = TRUE;
			break;
		      case 'a':
			command = ADD;
			break;
		      case 'l':
			command = LIST;
			break;
		      case 'C':
			command = CRON;
			break;
		      case 'v':
			verbose = TRUE;
			break;
		    }
		    break;
		    /*
		     * Single-char options that require parameters.
		     */
		  case 'D':
		  case 't':
		  case 'R':
		  case 'S':
		  case 'E':
		  case 'F':
		  case 'm':
		    p++;
		    if (!*p && argc > 1)
			--argc, p = *++argv;
		    else if (!*p) {
			char opt[2];
			opt[0] = c;
			opt[1] = '\0';
			errs = TRUE, err_optnoarg(opt);
		    }
		    /*
		     * Now c is the option and p is the parameter.
		     */
		    switch (c) {
		      case 'D':
			sfree((char *)dbpath); /* we know this is dynamic */
			dbpath = wp = smalloc(1+strlen(p));
			strcpy(wp, p);
			break;
		      case 't':
			e.type = name_to_type(p);
			if (e.type == INVALID_TYPE)
			    fatalerr_eventtype(p);
			break;
		      case 'R':
			{
			    char *q = p + strcspn(p, "/");
			    if (*q)
				*q++ = '\0';
			    else
				q = NULL;
			    e.period = parse_duration(p);
			    if (e.period == INVALID_DURATION)
				fatalerr_duration(p);
			    if (q) {
				e.length = parse_duration(q);
				if (e.length == INVALID_DURATION)
				    fatalerr_duration(q);
			    } else
				e.length = 0;
			}
			break;
		      case 'S':
			if (!parse_datetime(p, &e.sd, &e.st))
			    fatalerr_datetime(p);
			break;
		      case 'E':
		      case 'F':
			if (!parse_datetime(p, &e.ed, &e.et))
			    fatalerr_datetime(p);
			break;
		      case 'm':
			e.description = p;
			break;
		    }
		    p = NULL;	       /* prevent continued processing */
		    break;
		  default:
		    /*
		     * Unrecognised option.
		     */
		    {
			char opt[2];
			opt[0] = c;
			opt[1] = '\0';
			errs = TRUE, err_nosuchopt(opt);
		    }
		}
	    }
	} else {
	    /*
	     * A non-option argument.
	     */
	    if (nargs < lenof(args))
		args[nargs] = p;
	    nargs++;
	}
    }

    if (errs)
	exit(EXIT_FAILURE);
    if (nogo)
	exit(EXIT_SUCCESS);

    /*
     * Do the work.
     */
    switch (command) {
      case NONE:
	usage();
	break;
      case INIT:
	db_init();
	break;
      case ADD:
	caltrap_add(nargs, args, lenof(args), &e);
	break;
      case LIST:
	caltrap_list(nargs, args, lenof(args), verbose, machine_readable);
	break;
      case CRON:
	caltrap_cron(nargs, args, lenof(args));
	break;
      case DUMP:
	caltrap_dump(nargs, args, lenof(args));
	break;
      case LOAD:
	caltrap_load(nargs, args, lenof(args));
	break;
      case INFO:
	caltrap_info(nargs, args, lenof(args));
	break;
      case EDIT:
	caltrap_edit(nargs, args, lenof(args), &e);
	break;
      case DEL:
	caltrap_del(nargs, args, lenof(args));
	break;
    }

    db_close();

    return 0;
}
